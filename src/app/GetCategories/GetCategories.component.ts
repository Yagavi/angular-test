import { Component} from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import {GetCategoriesModels } from '../Models/GetCategories-Models.component';
import {ApiService} from '../service/api-service';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/Router';
import { ActivatedRoute } from '@angular/Router';
import { GetCategoriesList } from 'src/app/GetCategoriesList/GetCategoriesList.component';

@Component({
    selector:'GetCategories',
    templateUrl:'./GetCategories.component.html',
    styleUrls:['./GetCategories.component.css']

})

export class GetCategories implements OnInit{
GetCategoriesList:GetCategoriesModels[];
SelectedId:GetCategoriesModels;

    constructor(public Apiservice:ApiService,private router:Router){}

    ngOnInit():void{
        this.GetCategories();
    }

       GetCategories() :void
       {
           this.Apiservice.GetCategoriesService()
           .subscribe(
            resultArray=>   this.GetCategoriesList=resultArray,
            error=>console.log("error"+error)
           )
         //  console.log(this.GetCategoriesList);
       }         
    }
