import { Component} from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import {GetCategoriesModels } from '../Models/GetCategories-Models.component';
import {ApiService} from '../service/api-service';
import { Observable } from 'rxjs/internal/Observable';
import { GetCategories } from 'src/app/GetCategories/GetCategories.component';
import { Http } from '@angular/Http/src/http';
import { Router } from '@angular/Router';
import { ActivatedRoute } from '@angular/Router';

@Component({
    selector:'GetCategoriesList',
    templateUrl:'./GetCategoriesList.component.html',
    styleUrls:['./GetCategoriesList.component.css']

})

export class GetCategoriesList implements OnInit{
    id:number;
    GetCategoriesListById:GetCategoriesModels[];
    private sub:number;
    
        constructor(public Apiservice:ApiService,private Router:Router,private route:ActivatedRoute){}
    
        ngOnInit()
        {
            this.route.params.subscribe(id=>{this.sub=id['id']});        
            this.GetCategoriesByID(this.sub);
        }

        GetCategoriesByID(param:number)
        {          
                 this.Apiservice.GetCategoriesServiceByID(this.sub).subscribe(resultArray=>
                  {
                 console.log(this.sub);
                this.GetCategoriesListById=resultArray;
                console.log( this.GetCategoriesListById);
             });                        
        }           
     }
 