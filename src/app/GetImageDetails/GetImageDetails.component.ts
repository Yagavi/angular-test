import { Component} from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import {ApiService} from '../service/api-service';
import { Http } from '@angular/Http/src/http';
import { Router } from '@angular/Router';
import { ActivatedRoute } from '@angular/Router';
import { GetImageDetailsModels } from 'src/app/Models/GetImageDetails-Models.component';

@Component({
    selector:'GetImageDetails',
    templateUrl:'./GetImageDetails.component.html',
    styleUrls:['./GetImageDetails.component.css']

})

export class GetImageDetails implements OnInit{
    id:number;
    private sub:number;
    GetImageDetailsId:GetImageDetailsModels[];

    constructor(public Apiservice:ApiService,private Router:Router,private route:ActivatedRoute){}
    ngOnInit(){
        this.route.params.subscribe(id=>{this.sub=id['id']});        
        this.GetImageDetails(this.sub);
    }

    GetImageDetails(sub:number)
    {     
        this.Apiservice.GetImageDetails().subscribe(resultArray=>
            {
                this.GetImageDetailsId=resultArray;
            });                   
       }             
    }
