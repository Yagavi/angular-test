import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GetCategories } from './GetCategories/GetCategories.component';
import{HttpModule} from '@angular/Http';
import {FormsModule} from '@angular/forms';
import {RouterModule,Routes} from '@angular/Router';
import {approutes} from './app.routes.component';
import {ApiService} from './service/api-service';
import { GetCategoriesList } from 'src/app/GetCategoriesList/GetCategoriesList.component';
import {GetImageDetails} from 'src/app/GetImageDetails/GetImageDetails.component'


@NgModule({
  declarations: [
    AppComponent,
    GetCategories,
    GetCategoriesList,
    GetImageDetails
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(approutes)

  ],
  providers: [GetCategories,ApiService,GetCategoriesList],
  bootstrap: [AppComponent]
})
export class AppModule { }
