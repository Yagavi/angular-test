import {RouterModule,Routes} from '@angular/Router';
import { AppComponent } from './app.component';
import { GetCategories } from './GetCategories/GetCategories.component';
import { GetCategoriesList } from 'src/app/GetCategoriesList/GetCategoriesList.component';
import { GetImageDetails } from '../app/GetImageDetails/GetImageDetails.component';

export const approutes: Routes=[
    {path:'',component:GetCategories},
    {path:'GetCategories',component: GetCategories},
    {path:'GetCategories/:id',component: GetCategoriesList},
    {path:'GetImageDetails/:id',component:GetImageDetails}
]