import {Injectable} from '@angular/core';
import {HttpModule,Response,Http,RequestOptions,Headers} from '@angular/Http';
import 'rxjs/Rx';
import {Observable} from 'rxjs';
import {GetCategoriesModels} from '../Models/GetCategories-Models.component';
import {environment} from '../../environments/environment';
import {map } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { ActivatedRoute } from '@angular/Router';
import { GetImageDetailsModels } from 'src/app/Models/GetImageDetails-Models.component';

@Injectable()

export class ApiService{
    constructor(private _http:Http,private route:ActivatedRoute){ }
GetCategoriesUrl:string=environment.origin+"/api/GetImageGallery";
GetImageDetailsUrl:string=environment.origin+"/api/GetImageDetails";
id=this.route.snapshot.paramMap.get('id');

GetCategoriesService():Observable<GetCategoriesModels[]>{
    let getURL=this.GetCategoriesUrl;
    console.log(getURL);
    return this._http.get(getURL)
    .map((response : Response)=>{return <GetCategoriesModels[]> response.json()})
    .catch(error=>Observable.throw(error));

}

GetCategoriesServiceByID(id):Observable<GetCategoriesModels[]>{

   // console.log(id);
    let getURL=this.GetCategoriesUrl +'/'+id;
    console.log(getURL);
    return this._http.get(getURL)
    .map((response:Response)=>{return <GetCategoriesModels[]> response.json()})
    .catch(error=>Observable.throw(error));
}


GetImageDetails():Observable<GetImageDetailsModels[]>{

    // console.log(id);
     let getURL=this.GetImageDetailsUrl;
     console.log(getURL);
     return this._http.get(getURL)
     .map((response:Response)=>{return <GetImageDetailsModels[]> response.json()})
     .catch(error=>Observable.throw(error));
 }

}